//console.log("Esta funcionando, que tal");
//document.write("HOLA");
const express = require('express');
const app = express();
const port = 60000;
const path = require('path');   //PARA COMPATIBILIDAD CON BARRAS EN DIFERENTES S.O.
app.listen(port);
console.log(`Server is running on port ${port}`);
let productos = [
    {'id': 1, 'nombre': "Celular"},
    {'id': 2, 'nombre': "Notebook"},
    {'id': 3, 'nombre': "Mouse"},
    {'id': 4, 'nombre': "Teclado"},
    {'id': 5, 'nombre': "HDD Case"},
]


app.get('/productos', (request, response) => {
    console.log(productos);
    response.json(productos);
})

app.get('/', (request, response) => {
    //response.send("Hola mundo");
    console.log(__dirname);
    console.log(path.join(__dirname, '/html/index.html'));
    response.sendFile(path.join(__dirname, '/html/index.html'));
})


